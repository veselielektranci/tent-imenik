class CreateExtents < ActiveRecord::Migration
  def change
    create_table :extents do |t|
      t.string :start_number
      t.string :location

      t.timestamps
    end
    add_index :extents, :start_number, unique: true
  end
end
