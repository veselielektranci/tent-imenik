class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :user_name, index: true
      t.references :special_user, index: true
      t.references :job_class, index:true
      t.boolean :manual

      t.timestamps
    end
  end
end
