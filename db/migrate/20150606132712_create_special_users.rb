class CreateSpecialUsers < ActiveRecord::Migration
  def change
    create_table :special_users do |t|
      t.string :userName, limit: 80
      t.string :firstName, limit: 30
      t.string :lastName, limit: 50
      t.string :middleName, limit: 30
      t.string :phoneNumber, limit: 20
      t.string :department, limit: 80

      t.timestamps
    end
  end
end
