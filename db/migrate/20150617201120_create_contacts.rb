class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :type, limit: 10
      t.string :value, limit: 50
      t.references :link, index: true

      t.timestamps
    end
  end
end
