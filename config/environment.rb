# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

# URL to CISCO UNIFIED CALL MANAGER - USER DATA SERVICE
CUCM_UDS_URL = 'https://172.16.1.250:8443/cucm-uds'
# Max query response count to CUCUM UDS
CUCM_UDS_MAX_RESPONSE = 64
