class DirectoryContactsController < ApplicationController
  def index
  end

  def find
  end

  def login
  end

  def logout
  end

  def call
  end

  def hang_up
  end

  def pin
  end

  def unpin
  end

  def suggest
  end
  
  def sort
  end
end
