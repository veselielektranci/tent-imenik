class User < ActiveResource::Base
  self.site = CUCM_UDS_URL
  self.format = NodeXMLFormatter.new(self.element_name)
  self.include_format_in_path = false
  self.schema = {
    'userName' => :string,
    'firstName' => :string,
    'lastName' => :string,
    'middleName' => :string,
    'phoneNumber' => :string,
    'department' => :string }
  
  # veštački has_one ka Link ActiveRecord-u  
  def link
    @link = Link.find_by(user_id: userName).first if @link.nil?
  end
  
  def extent
    @extent = Extent.location_by_number(phoneNumber) if @extent.nil?
  end
    
  def self.find_by_name(anyName)
    find_by({ :name => anyName})
  end
  
  def self.find_by_number(phoneNumber)
    find_by({ :number => phoneNumber})
  end
  
  def self.find_by_numberLast(phoneNumberLast)
    find_by({ :numberlast => phoneNumberLast})
  end
  
  def self.find_by_name_and_number(anyName, phoneNumber)
    find_by({ :name => anyName, :number => phoneNumber})
  end
  
  def self.find_by_name_and_numberLast(anyName, phoneNumberLast)
    find_by({ :name => anyName, :numberlast => phoneNumberLast})
  end
  
  def self.find_by_name_and_number_and_numberLast(anyName, phoneNumber, phoneNumberLast)
    find_by({ :name => anyName, :number => phoneNumber, :numberlast => phoneNumberLast})
  end

  def self.find_by_username(username)
    find_by({ :username => username})
  end
  
  def self.find_by_first(firstName)
    find_by({ :first => firstName})
  end
  
  def self.find_by_last(lastName)
    find_by({ :last => lastName})
  end
  
  def self.find_by(params)
    User.find(:all, :params => pagged_params(params))
  end
    
  private
  
  def self.pagged_params(params)
    params.merge({ :start => 0, :max => CUCM_UDS_MAX_RESPONSE})
  end
  
end
