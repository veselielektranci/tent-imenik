class Contact < ActiveRecord::Base
  belongs_to :link
  
  def ip_telefon?
    type == 'CISCO'
  end
  
  def analogni_telefon?
    type == 'ANALOG'
  end
  
  def mobilni_telefon?
    type == 'MOBIL'
  end
    
  def e_adresa?
    type == 'EMAIL'
  end
  
  def adresa?
    type == 'ADDR'
  end
    
end
