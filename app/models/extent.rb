class Extent < ActiveRecord::Base
  scope :location_by_number, ->(number) { select(:location).where("INSTR(?,start_number) = 1", number).first }
  scope :all_locations, select(:location).distinct
end
