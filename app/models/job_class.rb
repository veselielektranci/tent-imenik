class JobClass < ActiveRecord::Base
    self.table_name = 'spa.sistematizacija'
    self.primary_key = 'sistematizacija_pk'
    # alias_attribute :new_column_name, :real_column_name
    establish_connection "ora_#{Rails.env}"
    has_many :employees, primary_key: "sistematizacija_pk", foreign_key: "sistematizacija_pk"
    belongs_to :company_unit, primary_key: "dpoc_pk", foreign_key: "dpoc_pk"
end
