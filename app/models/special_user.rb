class SpecialUser < ActiveRecord::Base
  has_one :link

  def extent
    @extent = Extent.location_by_number(phoneNumber) if @extent.nil?
  end

end