class Link < ActiveRecord::Base
  
  # veštački belongs_to ka User ActiveResource-u
  def user
    @user = User.find_by(userName: user_name).first if @user.nil?
  end
    
  belongs_to :specialUser
  
  belongs_to :jobClass, primary_key: "sistematizacija_pk"
  
  has_many :contacts
end
