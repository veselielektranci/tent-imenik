class CompanyUnit < ActiveRecord::Base
    self.table_name = 'spa.dpoc'
    self.primary_key = 'dpoc_pk'
    # alias_attribute :new_column_name, :real_column_name
    establish_connection "ora_#{Rails.env}"
    has_many :job_classes, primary_key: "dpoc_pk", foreign_key: "dpoc_pk"
    
    belongs_to :company_unit, primary_key: "dpoc_pk", foreign_key: "dpoc_pk_nad"
    has_many :company_units, primary_key: "dpoc_pk", foreign_key: "dpoc_pk_nad"
    
    def ogranak?
        tipdp_pk == "PD"
    end
    
    def pogon?
        tipdp_pk == "OGR"
    end
    
    def poslovi?
        tipdp_pk == "POSL"
    end
    
    def sektor?
        tipdp_pk == "SEKT"
    end
    
    def sluzba?
        tipdp_pk == "SLUZ"
    end
    
    def odeljenje?
        tipdp_pk == "ODEL"
    end
    
end
