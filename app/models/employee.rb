class Employee < ActiveRecord::Base
    self.table_name = 'spa.osoba'
    self.primary_key = 'osoba_pk'
    # alias_attribute :new_column_name, :real_column_name
    establish_connection "ora_#{Rails.env}"
    
    belongs_to :job_class, primary_key: "sistematizacija_pk", foreign_key: "sistematizacija_pk"
    belongs_to :company_unit, primary_key: "dpoc_pk", foreign_key: "dpoc_pk"
end
