$(document).on('click', 'span.clickable', function(e){
    var $this = $(this);
	if(!$this.hasClass('collapsed')) {
		$this.parents('.form-horizontal').find('.collapsable').slideUp();
		$this.addClass('collapsed');
		$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	} else {
		$this.parents('.form-horizontal').find('.collapsable').slideDown();
		$this.removeClass('collapsed');
		$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	}
})
