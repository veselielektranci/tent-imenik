require 'test_helper'

class DirectoryContactsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get find" do
    get :find
    assert_response :success
  end

  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get logout" do
    get :logout
    assert_response :success
  end

  test "should get call" do
    get :call
    assert_response :success
  end

  test "should get hang_up" do
    get :hang_up
    assert_response :success
  end

  test "should get pin" do
    get :pin
    assert_response :success
  end

  test "should get unpin" do
    get :unpin
    assert_response :success
  end

  test "should get suggest" do
    get :suggest
    assert_response :success
  end
  
  test "should get sort" do
    get :sort
    assert_response :success
  end

end
