class NodeXMLFormatter
  include ActiveResource::Formats::XmlFormat
  
  def initialize(node)
    @node = node
  end

  def decode(xml)
    ActiveResource::Formats::XmlFormat.decode(xml)[@node]
  end
end